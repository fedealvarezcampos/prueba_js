// # Ejercicio 1

// Escribe una función que devuelva un array de usuarios. De cada usuario guardaremos el username, el nombre y apellido, el sexo, el país, el email y un enlace a su foto de perfil.

// El número de usuarios a obtener se debe indicar con un parámetro de dicha función.

// Sírvete de la API: https://randomuser.me/

'use strict';

async function userList(numOfUsers) {
    try {
        const response = await fetch(
            `https://randomuser.me/api/?results=${numOfUsers}`
        );
        const data = await response.json();
        const users = data.results;

        const usersArray = [];

        for (const user of users) {
            const fullName = `${user.name.first} ${user.name.last}`;
            const username = user.login.username;
            const sex = user.gender;
            const country = user.location.country;
            const email = user.email;
            const userImage = user.picture.large;

            usersArray.push({
                username: username,
                fullname: fullName,
                sex: sex,
                country: country,
                email: email,
                image: userImage,
            });
        }

        console.log(usersArray);
    } catch (error) {
        return error;
    }
}

userList(3);
userList(5);
userList(8);
