// # Ejercicio 8
// Crea un cronómetro que permita ponerlo en marcha, pararlo y resetearlo.

'use strict';

const startButton = document.querySelector('.btt01');
const restartButton = document.querySelector('.btt02');
const chronoDiv = document.querySelector('.chrono');
chronoDiv.textContent = '00:00:00:000';

function chrono() {
    let time = 0;
    let diffInTime;
    let interval;

    const clockValue = time => {
        time = new Date(time);

        let hours = time.getHours() - 1;
        let minutes = time.getMinutes();
        let seconds = time.getSeconds();
        let milliseconds = time.getMilliseconds();

        hours = hours < 10 ? '0' + hours : hours;
        minutes = minutes < 10 ? '0' + minutes : minutes;
        seconds = seconds < 10 ? '0' + seconds : seconds;

        return `${hours}:${minutes}:${seconds}:${milliseconds}`;
    };

    const refresh = () => {
        let now = Date.now();
        let timePassed = now - diffInTime;
        diffInTime = now;

        if (this.isRunning) {
            time += timePassed;
        }

        chronoDiv.textContent = clockValue(time);
    };

    this.isRunning = false;

    this.start = function () {
        // bind para asignar el scope del setInterval que sería si no Window
        // https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_objects/Function/bind
        interval = setInterval(refresh.bind(this), 20);
        diffInTime = Date.now();
        this.isRunning = true;
    };

    this.stop = function () {
        clearInterval(interval);
        this.isRunning = false;
    };

    this.reset = function () {
        time = 0;
        chronoDiv.textContent = `00:00:00:000`;
    };
}

const watch = new chrono();

function start() {
    startButton.textContent = 'PAUSE';
    watch.start();
}

function stop() {
    startButton.textContent = 'START';
    watch.stop();
}

startButton.addEventListener('click', () =>
    watch.isRunning ? stop() : start()
);

restartButton.addEventListener('click', watch.reset);
