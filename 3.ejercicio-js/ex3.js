// # Ejercicio 3

// Crea un programa que reciba un número en decimal o binario y devuelva la conversión:

// -   Si le pasamos un nº en decimal debe retornar la conversión a binario.

// -   Si le pasamos un nº en binario deberá devolver su equivalente decimal.

// Para ello la función deberá recibir un segundo parámetro que indique la base: 10 (decimal) o 2 (binario).

// No se permite utilizar el método parseInt().

const converter = (num, base) => {
    if (base === 2) {
        return String(num)
            .split('')
            .reverse()
            .reduce(function (x, y, i) {
                return y === '1' ? x + Math.pow(2, i) : x;
            }, 0);
    } else if (base === 10) {
        let result = '';

        while (num > 0) {
            result += num % 2;
            num = Math.floor(num / 2);
        }

        result = result.split('').reverse().join('');
        return Number(result);
    } else {
        return console.log('Valor de base no permitido.');
    }
};

console.log(converter(10010110, 2));
console.log(converter(101010, 2));
console.log(converter(111011010, 2));
console.log(converter(15, 10));
console.log(converter(1234, 10));
console.log(converter(354, 10));
console.log(converter(354, 8));
