// # Ejercicio 7

// Crea una malla de cuadrados de tal forma que el color de cada uno de ellos se determine de forma aleatoria y vaya cambiando cada segundo.

// [Ejemplo](exemplo.png)

// Añade también un botón que permita añadir un nuevo cuadrado a la maya con las mismas propiedades que los anteriores.

'use strict';

const body = document.querySelector('body');
const button = document.createElement('div');

body.style.cssText = `
                    width: 320px;
                    margin: auto;
                    padding: 0;
                    height: 100%;
`;

button.classList.add('button');
button.textContent = 'Button';
button.style.cssText = `
                    text-align: center;
                    font-weight: bold;
                    width: 80px;
                    height: 20px;
                    margin: 20px auto 20px auto;
                    border-radius: 10px;
                    padding: 10px;
                    color: #fff;
                    background-color: #000;
`;
body.append(button);

const rows = document.createElement('div');
body.append(rows);

const rgbNamba = () => Math.round(Math.random() * 255);

const colorCubes = () => {
    rows.innerHTML = `
                    <div class="row"></div>
                    <div class="row"></div>
                    <div class="row"></div>
                    <div class="row"></div>
`;

    const innerRow = document.querySelectorAll('.row');

    for (const element of innerRow) {
        for (const i of innerRow) {
            const box = document.createElement('div');
            box.classList.add('box');

            box.style.cssText = `
            display: inline-block;
            width: 80px;
            height: 80px;
            background-color: rgb(${rgbNamba()}, ${rgbNamba()}, ${rgbNamba()});
        `;

            element.append(box);
        }
    }
};

const addCube = () => {
    const box = document.createElement('div');
    box.classList.add('box');

    box.style.cssText = `
            display: inline-block;
            width: 80px;
            height: 80px;
            background-color: rgb(${rgbNamba()}, ${rgbNamba()}, ${rgbNamba()});
        `;

    const boxColorChanger = () => {
        box.style.cssText = `
        display: inline-block;
        width: 80px;
        height: 80px;
        background-color: rgb(${rgbNamba()}, ${rgbNamba()}, ${rgbNamba()});
        `;
    };

    body.append(box);

    setInterval(boxColorChanger, 1000);
};

button.addEventListener('click', addCube);

colorCubes();
setInterval(colorCubes, 1000);
