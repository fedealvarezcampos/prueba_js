// # Ejercicio 6

// Subraya (manipulando el DOM) todas las palabras de los párrafos en "ejercicio.html" que contengan más de 5 letras.

const body = document.querySelector('body');
console.log(body);

// regex para evitar símbolos, espacios
const regex = /[!"',-.`\s+]/gm;
const words = body.textContent.replaceAll(regex, ' ').split(' ');

for (const word of words) {
    if (word.length > 5) {
        body.innerHTML = body.innerHTML.replaceAll(word, `<u>${word}</u>`);
    }
}
