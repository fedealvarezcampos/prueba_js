// # Ejercicio 4

// Escribe una función que, al recibir un array como parámetro, elimine los strings repetidos del mismo.

// No se permite hacer uso de Set ni Array.from().

const names = [
    'A-Jay',
    'Manuel',
    'Manuel',
    'Eddie',
    'A-Jay',
    'Su',
    'Miguel',
    'Reean',
    'Mona',
    'Manuel',
    'A-Jay',
    'Zacharie',
    'Zacharie',
    'Tyra',
    'Miguel',
    'Rishi',
    'Arun',
    'Kenton',
    'Mona',
    'Mona',
    'Mona',
];

const deleteRepeated = (array) =>
    array.filter((value, index) => array.indexOf(value) === index);

console.log(deleteRepeated(names));
