// # Ejercicio 2

// Crea un programa que imprima cada 5 segundos el tiempo desde la ejecución del mismo. Formatea el tiempo para que se muestren los segundos, los minutos, las horas y los días desde la ejecución.

const startTime = new Date();

console.log(startTime);

const excTime = () => {
    const timeTakenMs = new Date().getTime() - startTime.getTime();

    let seconds = new Date(timeTakenMs).getSeconds();
    let minutes = new Date(timeTakenMs).getMinutes();
    let hours = new Date(timeTakenMs).getHours() - 1;
    let days = Math.floor(timeTakenMs / 1000 / 86400);

    console.log(
        `Han pasado ${days} días, ${hours} horas, ${minutes} minutos y ${seconds} segundos.`
    );
};

setInterval(excTime, 5000);
