// # Ejercicio 5

// Consigue una lista con los nombres de los personajes de la serie Rick and Morty que aparecen en los episodios lanzados en el mes de enero (el año no importa).

// Utiliza llamadas a la API: 'https://rickandmortyapi.com/api/'

const getNames = async () => {
    try {
        const response = await fetch(
            `https://rickandmortyapi.com/api/character`
        );

        const data = await response.json();
        const pages = data.info.pages;

        const allCharacters = [];

        // Recorrer páginas y pushear todos los personajes
        // de todas las páginas al array allCharacters
        for (let i = 1; i <= pages; i++) {
            const response = await fetch(
                `https://rickandmortyapi.com/api/character?page=${i}`
            );
            const { results } = await response.json();
            allCharacters.push(...results);
        }

        const allCharactersJAN = [];
        // console.log(allCharactersJAN);

        // Recorrer el array con todos los personajes de antes
        // y si el valor del string de la fecha incluye "-01-" es de Enero.
        // Pushear sólo los nombres a un nuevo array, allCharactersJAN.
        for (let i = 0; i < allCharacters.length; i++) {
            const char = allCharacters[i];
            if (char.created.includes('-01-')) {
                allCharactersJAN.push(char.name);
            }
        }

        return allCharactersJAN;
    } catch (error) {
        return error;
    }
};

// Obtener el log con una función async
const getAsyncNamelist = async () => {
    try {
        const allCharactersJAN = await getNames();
        console.log(allCharactersJAN);
    } catch (error) {
        return error;
    }
};

getAsyncNamelist();
